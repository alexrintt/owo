#include <fstream>
#include <string>
#include <iostream>
#include <typeinfo>
#include <cstring>
#include <regex>
#include <vector>
#include <stdarg.h>
#include <unordered_map>

using namespace std;

template<typename T>
class Node {
public:
  T data;

  Node<T>* next;
  Node<T>* previous;
};

template<typename T>
class Iterator {
  bool reverse;

  Node<T>* cursor = NULL;
  Node<T>* source;

  bool relative_next() {
    if (is_empty()) return false;

    Node<T>* next = cursor == NULL ? source : reverse ? cursor->previous : cursor->next;

    if (next == NULL) return false;

    cursor = next;

    return true;
  }

public:
  Iterator(Node<T>* source, bool reverse) : source(source), reverse(reverse) {}

  void for_each(auto block) {
    while (next()) {
      block(cursor->data);
    }
  }

  bool next() {
    return relative_next();
  }

  T current() {
    return cursor->data;
  }

  bool has_next() {
    if (is_empty()) return false;

    if (cursor == NULL) return true;

    return cursor->next != NULL;
  }

  bool is_empty() {
    return source == NULL;
  }

  void reset() {
    cursor = NULL;
  }
};

template<typename T>
class LinkedList {
  unordered_map<int, Node<T>*> entries;

  Node<T>** top() { return &(entries[last_index()]); };
  Node<T>** bottom() { return &(entries[0]); };

  int length = 0;

  int last_index() {
    if (is_empty()) return 0;

    return length - 1;
  }

public:
  LinkedList() {
    *bottom() = NULL;
  }

  T operator[](int index) {
    return entries[index]->data;
  }

  void push(T data) {
    Node<T>* node = new Node<T>();

    node->data = data;
    node->previous = *top();
    node->next = NULL;

    if (*top() != NULL) {
      (*top())->next = node;
    }

    if (*bottom() == NULL) {
      *bottom() = node;
    }

    length++;

    *top() = node;
  }

  void push(vector<T> elements) {
    for (auto e : elements) push(e);
  }

  bool is_empty() {
    return length == 0;
  }

  void pop() {
    if (is_empty()) return;

    length--;

    *top() = *top()->previous;
    *top()->next = NULL;
  }

  void pop(int n) {
    for (int i = 0; i < n; i++) pop();
  }

  Iterator<T> iterator(bool reverse = false) {
    return reverse ? Iterator<T>(*top(), reverse) : Iterator<T>(*bottom(), reverse);
  }

  LinkedList<T> map(auto block) {
    Iterator<T> it = iterator();
    LinkedList<T> mappedlist;

    while (it.next()) {
      mappedlist.push(block(it.current()));
    }

    return mappedlist;
  }

  LinkedList<T> filter(auto block) {
    Iterator<T> it = iterator();
    LinkedList<T> filteredlist;

    while (it.next()) {
      T current = it.current();

      if (block(current)) filteredlist.push(current);
    }

    return filteredlist;
  }

  LinkedList<T> remove_where(auto block) {
    Iterator<T> it = iterator();
    LinkedList<T> filteredlist;

    while (it.next()) {
      T current = it.current();

      if (!block(current)) filteredlist.push(current);
    }

    return filteredlist;
  }

  void unshift(T data) {
    Node<T>* node = new Node<T>();

    node->data = data;
    node->next = *bottom();
    node->previous = NULL;

    if (*bottom() != NULL) {
      (*bottom())->previous = node;
    }

    unordered_map<int, Node<T>*> inc_entries;

    // Increment to each index (keys) of the map
    for (auto entry : entries) {
      inc_entries[entry.first + 1] = entries[entry.first];
    }

    entries = inc_entries;

    *bottom() = node;

    length++;
  }

  void insert_at(int index, T data) {
    if (index == 0) return unshift(data);

    if (index == last_index()) return push(data);

    if (index > last_index() || index < 0) return;

    Node<T>* node = new Node<T>();

    Node<T>* front = entries[index];
    Node<T>* back = front->previous;

    node->data = data;
    node->next = front;
    node->previous = back;

    if (back != NULL) back->next = node;
    if (front != NULL) front->previous = node;

    for (int i = last_index(); i >= index; i--) {
      entries[i + 1] = entries[i];
    }

    entries[index] = node;

    length++;
  }


  T reduce(auto block, T initial) {
    Iterator<T> it = iterator();
    T single = initial;

    while (it.next()) {
      single = block(single, it.current());
    }

    return single;
  }
};


int main() {
  LinkedList<int> numbers;

  numbers.push({ 1, 2, 3, 4, 5 });

  numbers.unshift(50);
  numbers.insert_at(1, 100);
  numbers.insert_at(1, 200);
  numbers.insert_at(1, 300);

  cout << "Index 1: " << numbers[1] << endl;

  cout << endl;

  LinkedList<int> squares = numbers.map([](int e) { return e * e; }).filter([](int e) { return e >= 10; }).remove_where([](int e) { return e <= 20; });

  Iterator<int> numbers_it = numbers.iterator();
  Iterator<int> squares_it = squares.iterator();

  cout << "Reduce: " << (numbers.reduce([](int ac, int e) { return ac + e; }, 0)) << endl;

  numbers_it.for_each([](int e) { cout << e << " "; });

  cout << endl;

  squares_it.for_each([](int e) { cout << e << " "; });

  return 0;
}