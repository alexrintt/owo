#include <fstream>
#include <string>
#include <iostream>
#include <typeinfo>
#include <cstring>
#include <regex>
#include <./runner.h>

using namespace std;

const int kIndentSize = 2;
const string kCommentChar = "#";

string trim(string str) {
  return regex_replace(str, regex("(^[ \t\n\v\f\r]+)|([ \t\n\v\f\r]+$)"), "");
}

// String parsers and lexers
string trim_left(string str) {
  return regex_replace(str, regex("(^[ \t\n\v\f\r]+)"), "");
}

int indentation_len(string line) {
  return line.length() - trim_left(line).length();
}

int indentation_level(int indent) {
  return indent / kIndentSize;
}

string erase_end_comment(string line) {
  return line.substr(0, line.find(kCommentChar));
}

string get_line_content(string line) {
  return erase_end_comment(trim(line));
}

// Exceptions
invalid_argument invalid_file(string file) {
  return invalid_argument("Can't find " + file + " or the file is empty");
}

invalid_argument unexpected_indent_exception(int line, string where) {
  return invalid_argument("Usage: 2036 run path/to/Script.se");
}

invalid_argument invalid_usage_exception() {
  return invalid_argument("Usage: 2036 run path/to/Script.se");
}

bool is_comment(string line) {
  return line[0] == kCommentChar[0];
}

bool is_newline(string line) {
  return line[0] == '\n';
}

bool should_ignore(string content) {
  return is_comment(content) || is_newline(content) || content.empty();
}

// Single file interpreter
int interpreter(vector<string> lines) {
  string content;

  int l = 0;
  auto ref = lines.begin();

  while (ref != lines.end()) {
    string line = erase_end_comment(*ref);
    string line_content = get_line_content(line);

    int indent = indentation_len(line);
    int indent_level = indentation_level(indent);

    if (indent % kIndentSize != 0) {
      throw unexpected_indent_exception(l, line);
    }

    l++;
    ref++;

    if (should_ignore(line_content)) continue;

    content += line + "\n";
  }

  cout << content << endl;

  return 0;
}

// Lexer starter implementation
enum Token {
  token_eof = -1,

  token_tuturu = -2,
  token_if = -3,
  token_else_if = -4,
  token_else = -5,

  token_declare = -6,
};

int get_token() {}

// CLI handler
int main(int argc, char** argv) {
  // CLI usage
  if (argc != 3) {
    throw invalid_usage_exception();
  }

  string command = argv[1];

  if (command != "run") {
    throw invalid_usage_exception();
  }

  vector<string> lines;
  string line;

  // Target file script
  ifstream file(argv[2]);

  // Use a while loop together with the getline() function to read the file line by line
  while (getline(file, line)) {
    lines.push_back(line);
  }

  if (!lines.size()) {
    throw invalid_file(argv[2]);
  }

  // Close the file
  file.close();

  // Run interpreter
  return interpreter(lines);
}