import Main from "./components/main";

window.addEventListener("DOMContentLoaded", function () {
  const main = Main("canvas");

  main.start();
});
