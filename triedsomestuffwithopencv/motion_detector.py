movement_detected_callback = lambda: None
no_movement_detected_callback = lambda: None

# From https://pyimagesearch.com/2015/05/25/basic-motion-detection-and-tracking-with-python-and-opencv/
def start_motion_detector(movement_cb=None, no_movement_cb=None):
    # import the necessary packages
    from os import write
    from random import random
    from imutils.video import VideoStream
    import argparse
    import datetime
    import imutils
    import time
    import cv2

    # - "Note: If you download the code to this post and intend to apply it to your own video files, you’ll likely need to tune the values for cv2.threshold and the --min-area argument to obtain the best results for your lighting conditions."
    # This file should contains the local config of your environment.
    import config as localconfig

    # construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-v", "--video", help="path to the video file")
    ap.add_argument("-a", "--min-area", type=int, default=500, help="minimum area size")
    args = vars(ap.parse_args())
    args["min_area"] = args["min_area"] or localconfig.MIN_AREA
    args["video"] = args["video"] or localconfig.VIDEO

    movement_cb = movement_cb or (lambda: None)
    no_movement_cb = no_movement_cb or (lambda: None)

    NO_MOVEMENT = 0
    MOVEMENT_DETECTED = 1

    STATE = NO_MOVEMENT

    is_static_video = args.get("video", None) is None

    # if the video argument is None, then we are reading from webcam
    if is_static_video:
        vs = VideoStream(src=0).start()
        time.sleep(2.0)
    # otherwise, we are reading from a video file
    else:
        vs = cv2.VideoCapture(args["video"])

    # initialize the first frame in the video stream
    firstFrame = None

    def new_state(current_state, new_state):
        if current_state == new_state:
            return current_state

        movement_cb() if new_state == MOVEMENT_DETECTED else no_movement_cb()

        return new_state


    while True:
        # grab the current frame and initialize the occupied/unoccupied
        # text
        key = cv2.waitKey(1) & 0xFF
        frame = vs.read()
        frame = frame if is_static_video else frame[1]
        STATE = new_state(STATE, NO_MOVEMENT)
        # if the frame could not be grabbed, then we have reached the end
        # of the video
        if frame is None:
            break
        # resize the frame, convert it to grayscale, and blur it
        frame = imutils.resize(frame, width=500)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (21, 21), 0)
        # if the first frame is None, initialize it
        if firstFrame is None:
            firstFrame = gray
            continue

        # compute the absolute difference between the current frame and
        # first frame
        frameDelta = cv2.absdiff(firstFrame, gray)
        thresh = cv2.threshold(frameDelta, 45, 255, cv2.THRESH_BINARY)[1]
        # dilate the thresholded image to fill in holes, then find contours
        # on thresholded image
        thresh = cv2.dilate(thresh, None, iterations=2)
        cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_SIMPLE)
        raw_cnts = imutils.grab_contours(cnts)
        cnts = []

        for c in raw_cnts:
            (x, y, w, h) = cv2.boundingRect(c)

            # Ignore sky (wall and above)
            if y + h <= 150:
                continue

            cnts.append(c)

        total_area = 0

        TOTAL_AREA = 500 * 500
        LUMINANCE_THRESHOLD = 40000

        for c in cnts:
            total_area = total_area + cv2.contourArea(c)
            if total_area >= LUMINANCE_THRESHOLD: break

        if total_area >= LUMINANCE_THRESHOLD: 
            firstFrame = gray
            continue
        
        for c in cnts:
            # if the contour is too small,ignore  it
            if cv2.contourArea(c) < args["min_area"]:
                continue
            # compute the bounding box for the contour, draw it on the frame,
            # and update the text
            (x, y, w, h) = cv2.boundingRect(c)

            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
            STATE = new_state(STATE, MOVEMENT_DETECTED)

        if STATE == MOVEMENT_DETECTED:
            print(total_area)
                # draw the text and timestamp on the frame
        cv2.putText(frame, "Status: {}".format(STATE), (10, 20),
            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
        cv2.putText(frame, datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
            (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)
        # show the frame and record if the user presses a keywwwwwwwwwwwwwwww
        cv2.imshow("Security Feed", frame)
        cv2.imshow("Thresh", thresh)
        cv2.imshow("Frame Delta", frameDelta)
        # if the `q` key is pressed, break from the lop
        if key == ord("q"):
            break
    # cleanup the camera and close any open windows
    vs.stop() if is_static_video else vs.release()
    cv2.destroyAllWindows()

start_motion_detector()
