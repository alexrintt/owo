function internalConfig() {
  // Only change if you are modifying the script to fill your needs
  // otherwise there's no need to edit these configs
  const config = {
    ZIP_DOWNLOAD_FOLDER: `./data/zips`,
    ZIP_EXTRACTION_FOLDER: `./data/fullsource`,
  };

  return config;
}

export function globalConfig() {
  // Change as you wish
  const config = {
    REPO_COUNT: 146,
    OLD_USERNAME: `lakscastro`,
    NEW_USERNAME: `alexrintt`,
    OLD_DISPLAY_NAME: `Laks Castro`,
    NEW_DISPLAY_NAME: `Alex Rintt`,
    ...internalConfig(),
  };

  return config;
}
