export default [
  "Update old username",
  "Replace with new username",
  "Update project by replacing old username",
  "Remove old username references",
  "Replace old username reference",
];
