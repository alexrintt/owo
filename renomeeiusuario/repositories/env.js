import dotenv from "dotenv";
import { Octokit } from "octokit";

export const { parsed: env } = dotenv.config();
