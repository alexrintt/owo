import { globalConfig } from "./global-config.js";
import { octokit } from "./github-client.js";

export async function fetchRepositories() {
  const config = globalConfig();
  let offset = 0;
  const data = [];
  const repoCount = config.REPO_COUNT;

  while (offset < repoCount) {
    const response = await octokit.graphql(
      `
      query GetRepos($cursor: String) { 
        viewer {
          login
          repositories(after: $cursor, first: 100, orderBy: {field: PUSHED_AT,direction: DESC}) {
            edges {
              cursor
              node {
                url
                homepageUrl
                id
                name
                owner {
                  login
                }
              }
            }
          }
        }
      }
    `,
      {
        cursor: data.length > 0 ? data[data.length - 1].cursor : undefined,
      }
    );

    data.push(
      ...response.viewer.repositories.edges.map((e) => ({
        ...e.node,
        slug: `${e.node.owner.login}/${e.node.name}`,
      }))
    );

    offset += 100;
  }

  return data;
}

export async function changeRepoHomepageUrl(repoId, homepageUrl) {
  const query = `
    mutation ChangeRepoUrl($repoId: ID!, $homepageUrl: URI) {
      updateRepository(input: {repositoryId: $repoId, homepageUrl: $homepageUrl}) {
        clientMutationId
        repository {
          url
          homepageUrl
          id
          name
          owner {
            login
          }
        }
      }
    }
  `;

  const variables = {
    repoId,
    homepageUrl,
  };

  await octokit.graphql(query, variables);
}

export async function replaceReposHomepageUrl(repos) {
  const config = globalConfig();

  for (const repo of repos) {
    if (!repo.slug.startsWith(config.NEW_USERNAME)) continue;
    const repoId = repo.id;
    const homepageUrl = repo.homepageUrl;

    if (!homepageUrl) continue;

    const regex = new RegExp(config.OLD_USERNAME, `gi`);
    const newHomepageUrl = homepageUrl.replace(regex, config.NEW_USERNAME);

    await changeRepoHomepageUrl(repoId, newHomepageUrl);

    console.log(
      `Changed successfully ${repo.slug} with new homepage url: ${newHomepageUrl}`
    );
  }
}

// console.log(
//   "Hello, %s",
//   JSON.stringify(
//     repos
//       .map((repo) => repo.node.url)
//       .filter((repo) => repo.includes("alexrintt/")).length,
//     0,
//     2,
//     null
//   )
// );
