import { Octokit } from "octokit";
import { env } from "./env.js";

export const octokit = new Octokit({
  auth: env.GITHUB_TOKEN,
});
