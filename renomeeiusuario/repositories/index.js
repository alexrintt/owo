import {
  fetchRepositories,
  replaceReposHomepageUrl,
} from "./replace-homepage-url.js";

import { downloadRepositories } from "./download-repositories.js";

const repos = await fetchRepositories();

// await replaceReposHomepageUrl(repos);
await downloadRepositories(repos);
