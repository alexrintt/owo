// import { downloadRemoteFile } from "./download-remote-file";
import { octokit } from "./github-client.js";
import { downloadRemoteFile, extractZip } from "./download-remote-file.js";
import path from "path";
import { globalConfig } from "./global-config.js";
import { resolveFileLocation } from "./file-utils.js";
import fs from "fs";
import chalk from "chalk";
import { env } from "./env.js";
import fetch from "node-fetch";

function getUsername(repoSlug) {
  return repoSlug.split("/")[0];
}

export async function downloadRepositories(repos) {
  const result = {};
  const errors = {};
  const skipped = {};

  const thereResults = () =>
    [skipped, errors, result].some((m) => Object.keys(m).length !== 0);

  for (let i = 0, t = 1; i < repos.length; i++) {
    const repo = repos[i];
    const slug = repo.slug;
    // const slug = "alexrintt/rubikasciin";
    const username = getUsername(slug);

    try {
      if (t >= 10) {
        console.log(
          `The download of ${chalk.red(slug)} wasn't possible. Skipping it.`
        );
        t = 1;
        continue;
      }
      console.log(
        `${chalk.yellowBright(`Trying `)} ${chalk.white.bgMagenta(
          ` ${t}° `
        )} attempt`
      );

      const repoRef = await getRepositoryDefaultBranch(slug);

      const isEmpty = await isRepoEmpty(slug, repoRef);

      if (isEmpty) {
        console.log(
          chalk.yellowBright(
            `Empty repository ${chalk.blueBright(
              slug
            )} at ref ${chalk.cyanBright(repoRef)}, skipping...`
          )
        );

        skipped[username] = [...new Set([...(skipped[username] ?? []), slug])];

        continue;
      }

      const downloadedZipFilepath = await downloadRepository(slug, repoRef);
      await extractRepositoryZip(downloadedZipFilepath, slug, repoRef);

      result[username] = [...new Set([...(result[username] ?? []), slug])];

      t = 1;
    } catch (e) {
      console.log(`Something went wrong trying again...`);
      t++;
      i--;
      errors[username] = [...new Set([...(errors[username] ?? []), slug])];
    } finally {
      if (thereResults()) console.log(`\n`);
    }
  }

  if (!thereResults()) {
    console.log(chalk.grey(`There's no results...`));
  } else {
    console.log(
      chalk.italic.yellowBright(`Finished tasks and there's the results:`)
    );
  }

  for (const username in result) {
    console.log(
      `${chalk.greenBright(`Success downloaded`)} ${chalk.white.bgGreen(
        ` ${result[username].length} `
      )} repositories of ${chalk.white.bgBlue(username)} account`
    );

    if (!!errors[username]) {
      console.log(
        `${chalk.redBright(`But failed for the following repositories: `)}`
      );
      for (const repoErr of errors[username]) {
        console.log(chalk.white.bgRed(repoErr));
      }
    }
    if (!!skipped[username]) {
      console.log(
        `${chalk.cyanBright(`And skipped the following (empty repos):`)}`
      );
      for (const skippedRepo of skipped[username]) {
        console.log(chalk.white.bgCyan(skippedRepo));
      }
    }
  }

  for (const username in errors) {
    console.log(`\n`);

    if (!!result[username]) continue;

    console.log(
      `${chalk.redBright(
        `Failed to download the following repositories (${chalk.white.bgBlue(
          username
        )}): `
      )}`
    );

    for (const repoErr of errors[username]) {
      console.log(chalk.white.bgRed(repoErr));
    }
  }

  for (const username in skipped) {
    console.log(`\n`);

    if (!!result[username]) continue;

    console.log(
      `${chalk.cyanBright(
        `Skipped download of the following repositories (${chalk.white.bgBlue(
          username
        )}) since it's empty: `
      )}`
    );

    for (const emptyRepo of skipped[username]) {
      console.log(chalk.white.bgCyan(emptyRepo));
    }
  }
}

export function generateOwnerRepoFolderName(repoSlug) {
  return repoSlug.replace(`/`, `-`);
}

export function getRepoName(repoSlug) {
  return repoSlug.split("/")[1];
}

export async function extractRepositoryZip(zipFilepath, repoSlug, repoRef) {
  const config = globalConfig();
  const destinationPath = path.join(
    resolveFileLocation(config.ZIP_EXTRACTION_FOLDER),
    generateOwnerRepoFolderName(repoSlug)
  );

  await extractZip(zipFilepath, destinationPath);
  await removeRefWrapFolder(destinationPath, repoSlug, repoRef);
}

export async function removeRefWrapFolder(folder, repoSlug, repoRef) {
  const filenames = fs.readdirSync(folder);

  for (const filename of filenames) {
    const wrapPath = path.join(folder, filename);

    if (!fs.statSync(wrapPath).isDirectory()) continue;
    if (filename !== `${getRepoName(repoSlug)}-${repoRef}`) continue;

    const contentFilenames = fs.readdirSync(wrapPath);

    for (const filename of contentFilenames) {
      const contentpath = path.join(wrapPath, filename);
      const newContentPath = path.join(folder, filename);

      const alreadyExists = fs.existsSync(newContentPath);

      if (alreadyExists) {
        const isDir = fs.statSync(newContentPath).isDirectory();

        if (isDir) {
          fs.rmdirSync(newContentPath, { recursive: true });
        } else {
          fs.unlinkSync(newContentPath);
        }
      }

      fs.renameSync(contentpath, newContentPath);
    }

    fs.rmdirSync(wrapPath, { recursive: true });

    console.log(
      `${chalk.green(`Successfully`)} downloaded ${chalk.white.bgBlue.bold(
        repoSlug
      )} with ref ${chalk.cyanBright.bold(repoRef)}`
    );
  }
}

export async function downloadRepository(repoSlug, repoRef) {
  const config = globalConfig();

  const downloadUrl = generateDownloadUrl(repoSlug, repoRef);

  console.log(`Downloading repo url: ${chalk.blueBright(downloadUrl)}`);

  return await downloadRemoteFile(
    downloadUrl,
    path.join(
      config.ZIP_DOWNLOAD_FOLDER,
      `${generateOwnerRepoFolderName(repoSlug)}.zip`
    )
  );
}

export function generateDownloadUrl(repoSlug, ref) {
  return `https://github.com/${repoSlug}/archive/refs/heads/${ref}.zip`;
}

export async function getRepositoryDefaultBranch(repoSlug) {
  const {
    data: { default_branch: defaultBranch },
  } = await octokit.request(`/repos/${repoSlug}`);

  return defaultBranch;
}

export async function isRepoEmpty(repoSlug, repoRef) {
  const url = `https://api.github.com/repos/${repoSlug}/git/trees/${repoRef}`;

  try {
    const response = await fetch(url, {
      headers: {
        Authorization: `Bearer ${env.GITHUB_TOKEN}`,
      },
    });

    const totalEmpty = response.status === 409;
    const emptyInTheDefaultBranch = response.status === 404;

    return totalEmpty || emptyInTheDefaultBranch;
  } catch (e) {
    return true;
  }
}
