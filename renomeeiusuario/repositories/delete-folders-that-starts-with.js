import fs from "fs";
import path from "path";

const folder = path.resolve("./data/fullsource");

for (const filename of fs.readdirSync(folder)) {
  if (filename.startsWith("alexrintt")) continue;

  fs.rmdirSync(path.join(folder, filename), { recursive: true });
}
