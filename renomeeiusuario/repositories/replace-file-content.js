import process from "process";
import fs from "fs";
import path from "path";

// const [filename, ...pairs] = process.argv.slice(2);

// const targetFileLocation = path.join(path.resolve(), filename);

// replaceFileContents(targetFileLocation, pairs);
// replaceFileContents(targetFileLocation, pairs);

export function replaceFileContents(filename, pairs) {
  fs.readFile(filename, `UTF-8`, function (err, fileContents) {
    if (err) {
      return console.log(`Something went wrong: ${err}`);
    }

    for (let i = 0; i < pairs.length; i += 2) {
      const [replaceThis, withThis] = [pairs[i], pairs[i + 1]];

      fileContents = replaceString(fileContents, replaceThis, withThis);
    }

    fs.writeFileSync(filename, fileContents);
  });
}

export function replaceString(source, searchFor, replaceWith) {
  if (!searchFor || !replaceWith) return source;

  const regex = new RegExp(searchFor, `g`);

  return source.replace(regex, replaceWith);
}
