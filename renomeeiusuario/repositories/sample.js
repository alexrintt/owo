import fs from "fs";
import path from "path";

const a = path.normalize(
  path.join(path.resolve(), path.normalize("./a/b.txt"))
);

console.log(path.dirname(a));
