import https from "https";
import fs from "fs";
import {
  createFileAndFolderRecursivelyIfNotExists,
  resolveFileLocation,
} from "./file-utils.js";
import request from "request";
import path from "path";
import extract from "extract-zip";
import { env } from "./env.js";

export async function downloadRemoteFile(fileUrl, destinationFile) {
  const downloadedFilepath = resolveFileLocation(destinationFile);

  await createFileAndFolderRecursivelyIfNotExists(downloadedFilepath);

  const downloadedFileStream = fs.createWriteStream(downloadedFilepath);

  return new Promise((res, rej) => {
    request(fileUrl, {
      headers: {
        Authorization: `Bearer ${env.GITHUB_TOKEN}`,
      },
    }).pipe(downloadedFileStream);

    downloadedFileStream.on(`finish`, () => {
      downloadedFileStream.close();
      res(downloadedFilepath);
    });
  });
}

export async function unzipFiles(dirPath, destinationFolder) {
  const files = fs.readdirSync(dirPath);
  const absDestinationFolder = resolveFileLocation(destinationFolder);

  await Promise.all(
    files.map(async (file) => {
      const filepath = path.join(dirPath, file);
      if (fs.statSync(filepath).isDirectory()) {
        await unzipFiles(filepath);
      } else {
        const fullFilePath = path.join(dirPath, file);
        const folderName = file.replace(`.zip`, ``);
        if (path.extname(file) === `zip`) {
          await extractZip(
            fullFilePath,
            path.join(absDestinationFolder, folderName)
          );
          await unzipFiles(path.join(dirPath, folderName));
        }
      }
    })
  );
}

export async function extractZip(source, target) {
  try {
    await extract(source, { dir: target });
  } catch (err) {
    console.log("Oops: extractZip failed", err);
  }
}
