import path from "path";
import fs from "fs";

export function currentFolder() {
  return path.resolve();
}

export function isWindows() {
  return os.platform() === `win32`;
}

export function resolveFileLocation(filepath) {
  return path.normalize(path.join(currentFolder(), path.normalize(filepath)));
}

export function resolveFileParts(fullpath) {
  return {
    filename: path.basename(fullpath),
    parentFolder: path.dirname(fullpath),
  };
}

export async function createFileAndFolderRecursivelyIfNotExists(absFilepath) {
  const fileParts = resolveFileParts(absFilepath);
  const folderpath = fileParts.parentFolder;

  if (!fs.existsSync(folderpath)) {
    await createFolderRecursively(folderpath);
  }

  if (!fs.existsSync(absFilepath)) {
    fs.writeFileSync(absFilepath, ``);
  }
}

export function createFolderRecursively(path) {
  return new Promise((res, rej) => {
    fs.mkdir(path, { recursive: true }, (err, path) => {
      if (err) return rej(err);
      res(path);
    });
  });
}

export async function writeFileContents(filepath, contents) {
  await createFileAndFolderRecursivelyIfNotExists(filepath);

  const path = resolveFileLocation(filepath);

  return new Promise((res, rej) => {
    fs.writeFileSync(path, contents);
    res();
  });
}

export function readFileContents(filepath, contents) {
  const path = resolveFileLocation(filepath);

  return new Promise((res, rej) => {
    fs.readFile(path, `UTF-8`, function (err, fileContents) {
      if (err) return rej(err);
      else res(fileContents);
    });
  });
}
