import { Octokit } from "octokit";
import dotenv from "dotenv";
import shell from "shelljs";
import path from "path";
import { existsSync, mkdirSync, readdirSync } from "fs";

export const { parsed: env } = dotenv.config();

export const octokit = new Octokit({
  auth: env.GITHUB_TOKEN,
});

function internalConfig() {
  // Only change if you are modifying the script to fill your needs
  // otherwise there's no need to edit these configs
  const config = {
    ZIP_DOWNLOAD_FOLDER: `./data/zips`,
    ZIP_EXTRACTION_FOLDER: `./data/fullsource`,
  };

  return config;
}

export function globalConfig() {
  // Change as you wish
  const config = {
    // REPO_COUNT: 246,
    OLD_USERNAME: `lakscastro`,
    NEW_USERNAME: `alexrintt`,
    OLD_DISPLAY_NAME: `Laks Castro`,
    NEW_DISPLAY_NAME: `Alex Rintt`,
    ...internalConfig(),
  };

  return config;
}

export async function fetchRepositories() {
  const data = [];
  let currentResults;

  while (!currentResults || currentResults.length === 100) {
    const response = await octokit.graphql(
      `
      query GetRepos($cursor: String) { 
        viewer {
          login
          repositories(after: $cursor, first: 100, orderBy: {field: PUSHED_AT,direction: DESC}) {
            edges {
              cursor
              node {
                url
                homepageUrl
                id
                name
                slug: nameWithOwner
                owner {
                  login
                }
                defaultBranchRef {
                  name
                }
              }
            }
          }
        }
      }
    `,
      {
        cursor:
          currentResults?.length ?? 0 > 0
            ? currentResults[currentResults.length - 1].cursor
            : undefined,
      }
    );

    currentResults = response.viewer.repositories.edges;

    data.push(
      ...currentResults
        .map((e) => ({
          ...e.node,
          owner: e.node.owner.login,
          foldername: e.node.slug.replace("/", "-"),
          repoRef: e.node.defaultBranchRef?.name,
        }))
        .filter((e) => !!e.repoRef)
        .map((e) => ({
          ...e,
          location: path.join(getUserPath(e), e.foldername),
        }))
    );
  }

  return data;
}

function getUserPath(repo) {
  return path.normalize(path.resolve(`./repos/${repo.owner}`));
}

const repos = await fetchRepositories();

for (const repo of repos) {
  const newHomepageUrl = repo.homepageUrl?.replace(/lakscastro/gi, "alexrintt");

  if (repo.owner !== "alexrintt") continue;
  try {
    await changeRepoHomepageUrl(repo.id, newHomepageUrl);
    console.log(
      "Changing repo url: [" + repo.slug + "] with [" + newHomepageUrl + "]"
    );
  } catch (e) {}
}

// await cloneAllRepositories();
// await commitAndPushChanges();

async function cloneAllRepositories() {
  for (const repo of repos) {
    const userpath = getUserPath(repo);

    if (!existsSync(userpath)) {
      mkdirSync(userpath, { recursive: true });
    }

    shell.exec(`git clone ${repo.url}`, {
      cwd: userpath,
    });
  }
}

async function commitAndPushChanges() {
  const wrapper = path.resolve("./repos/alexrintt");

  const repoFolderNames = readdirSync(wrapper);

  const messages = [
    "Update old username",
    "Replace with new username",
    "Update project by replacing old username",
    "Remove old username references",
    "Replace old username reference",
    "Update name reference",
    "Migrate repository user login",
    "Replace old user login",
    "Replace with new repository user login",
    "Remove legacy username",
  ];

  function randomInt(min, max) {
    const delta = max - min;

    return Math.floor(Math.random() * delta + min);
  }

  function generateMessage() {
    return messages[randomInt(0, messages.length)];
  }

  for (const repoFolderName of repoFolderNames) {
    const workingDir = path.join(wrapper, repoFolderName);
    const [repo] = repos.filter(
      (r) => r.name === repoFolderName && r.owner === "alexrintt"
    );

    if (!repo) continue;

    const message = generateMessage();
    const add = `git add .`;
    const commit = `git commit -m "${message}"`;
    const push = `git push origin ${repo.repoRef}`;

    shell.exec(add, { cwd: workingDir });
    shell.exec(commit, { cwd: workingDir });
    shell.exec(push, { cwd: workingDir });
  }
}

async function changeRepoHomepageUrl(repoId, homepageUrl) {
  const query = `
    mutation ChangeRepoUrl($repoId: ID!, $homepageUrl: URI) {
      updateRepository(input: {repositoryId: $repoId, homepageUrl: $homepageUrl}) {
        clientMutationId
        repository {
          url
          homepageUrl
          id
          name
          owner {
            login
          }
        }
      }
    }
  `;

  const variables = {
    repoId,
    homepageUrl,
  };

  await octokit.graphql(query, variables);
}
