from socket import socket
from threading import Thread
import os

HOST = '127.0.0.1'
PORT = 3479

BUFFER_1MB = 1024
BUFFER_2MB = BUFFER_1MB * 2

connections = []

tcp = socket()
orig = (HOST, PORT)
tcp.bind(orig)
tcp.listen(1)


# Source: https://stackoverflow.com/questions/2084508/clear-terminal-in-python
def clear():
    '''
    Clears the terminal screen and scroll back to present
    the user with a nice clean, new screen. Useful for managing
    menu screens in terminal applications.
    '''
    os.system('cls||echo -e \\\\033c')

def accept_tcp_connection():
    client, address = tcp.accept()
    connections.append((client, address))
    print_menu()


def print_menu():    
    clear()

    print('Connections:')

    for i in range(0, len(connections)):
        _, address = connections[i]
        print(f'{i}. {address}')

    if not len(connections):
        print('No connections yet...')

    if output is not None and len(output):
        print(f'\n\n{output.decode().encode("utf-8").decode("unicode-escape")}\n')

    print('Your instruction: ')


def capture_input():
    global output
    output = None

    while True:
        instruction = input().strip()

        if not instruction: return

        if instruction in 'rR':
            pass
        else:
            try:
                index, command = instruction.split(' ')

                client, address = connections[int(index)]

                client.send(command.encode())

                output = client.recv(BUFFER_2MB)
            except ValueError:
                pass
        print_menu()

ui_thread = Thread(target=print_menu)
ui_thread.start()

tcp_thread = Thread(target=accept_tcp_connection)
tcp_thread.start()

capture_input()