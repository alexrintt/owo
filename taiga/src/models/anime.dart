class Anime {
  String title;
  String synopsis;
  String url;
  String imgURL;
  String id;

  Anime({this.title, this.synopsis, this.url, this.imgURL, this.id});
}
