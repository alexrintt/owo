import '../config/bot.dart';

class Initializer {
  constructor() {}

  void start() {
    BotConfig.define();
  }
}
