> ## In Development

### A Simple Music Player with Steins;Gate Soundtrack's

Written in Vanilla JavaScript, with Web Audio API  
UI/UX **based on [Samsung Music Mobile App](https://play.google.com/store/apps/details?id=com.sec.android.app.music&hl=pt_BR)**

### Like Steins;Gate?

[Enjoy](https://steins.netlify.com)

### Like JavaScript?

[Enjoy](https://github.com/alexrintt/steins-gate-soundtrack)
