import 'package:flutter/material.dart';

class VideoData {
  final String urlHd;
  final String url;
  final String episodeId;

  VideoData({this.urlHd, @required this.url, @required this.episodeId});
}
