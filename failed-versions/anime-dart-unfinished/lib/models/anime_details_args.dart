import 'package:flutter/material.dart';

class AnimeDetailsArgs {
  final String animeId;
  final String title;

  AnimeDetailsArgs({@required this.animeId, @required this.title});
}
