import 'package:flutter/material.dart';

class EpisodeInfoData {
  final String label;
  final String id;
  final String animeId;

  EpisodeInfoData(
      {@required this.label, @required this.id, @required this.animeId});
}
