## Release Inacabada

E não é nem por desmotivação, apenas cometi muitos erros que comprometeram o projeto a ponto de ser mais fácil iniciar um novo do que consertar os bugs atuais. Afinal esta release inacabada é apenas para fins nostálgicos no futuro, visto que não recomendo instalar ele; esse foi o primeiro código que escrevi em Flutter sem ser com o objetivo Foo Bar; então o código e as Views/Widgets estão uma bagunça indeterminável; coisa essa que será corrigida [neste repositório](https://github.com/alexrintt/anime-dart). Apesar de que com estes erros consegui aprender bastante, não prometo corrigir 100%, afinal será minha segunda tentativa, infelizmente não sou um gênio que erra uma vez e na segunda tentativa acerta tudo, então com o passar do tempo e das tentativas vou melhorando, mas a data final de release para este novo repositório ainda é 10/08!

## O que está pronto?

Apesar de estar com um código totalmente sem elaboração, algumas features ainda estão funcionando. E segue abaixo a print de algumas páginas.

#### Dark Mode

<table>
  <thead>
    <th>
      <img src="/assets/d-home.jpeg" width="150">
    </th>
    <th>
      <img src="/assets/d-episodes.jpeg" width="150">
    </th>
  </thead>
  <tbody>
    <td>
      Página Inicial
    </td>
    <td>
      Listagem de episódios
    </td>
  </tbody>
</table>

<table>
  <thead>
    <th>
      <img src="/assets/d-search.jpeg" width="150">
    </th>
    <th>
      <img src="/assets/d-search-ep.jpeg" width="150">
    </th>
  </thead>
  <tbody>
    <td>
      Página de buscar animes
    </td>
    <td>
      Feature de filtrar episódios
    </td>
  </tbody>
</table>

<table>
  <thead>
    <th>
      <img src="/assets/d-search-with-results.jpeg" width="150">
    </th>
    <th>
      <img src="/assets/d-watch.jpeg" width="150">
    </th>
  </thead>
  <tbody>
    <td>
      Página de buscar animes com resultados
    </td>
    <td>
      Página de assistir um anime
    </td>
  </tbody>
</table>

#### Light Mode

<table>
  <thead>
    <th>
      <img src="/assets/l-home.jpeg" width="150">
    </th>
    <th>
      <img src="/assets/l-episodes.jpeg" width="150">
    </th>
  </thead>
  <tbody>
    <td>
      Página Inicial
    </td>
    <td>
      Listagem de episódios
    </td>
  </tbody>
</table>

<table>
  <thead>
    <th>
      <img src="/assets/l-search.jpeg" width="150">
    </th>
    <th>
      <img src="/assets/l-search-ep.jpeg" width="150">
    </th>
  </thead>
  <tbody>
    <td>
      Página de buscar animes
    </td>
    <td>
      Feature de filtrar episódios
    </td>
  </tbody>
</table>

<table>
  <thead>
    <th>
      <img src="/assets/l-search-with-results.jpeg" width="150">
    </th>
    <th>
      <img src="/assets/l-watch.jpeg" width="150">
    </th>
  </thead>
  <tbody>
    <td>
      Página de buscar animes com resultados
    </td>
    <td>
      Página de assistir um anime
    </td>
  </tbody>
</table>

## Isso é tudo pessoal

O projeto morre aqui, mas renasce das cinzas [neste repositório](https://github.com/alexrintt/anime-dart), mais completo e mais bem estruturado

<br>
<br>
<br>
<br>

<h2 align="center">
  Open Source
</h2>
<p align="center">
  <sub>Copyright © 2020-present, Alex Rintt.</sub>
</p>
<p align="center">Anime Dart <a href="https://github.com/alexrintt/anime-dart/blob/master/LICENSE.md">is MIT licensed 💖</a></p>
<p align="center">
  <img src="/assets/flutter.png" width="35" />
</p>
