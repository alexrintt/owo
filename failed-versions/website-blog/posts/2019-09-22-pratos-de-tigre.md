---
title: Pratos de Tigre
description: 'Um prato de trigo para trinta tristes trigos '
date: '2019-09-22 10:03:49'
image: /assets/img/lake.jpg
category: LS
background: deepskyblue
---
# Introdução

Corpo do meu texto da introdução

# Desenvolvimento

Corpo do meu desenvolvimento

> Este é um bloquote que eu acho bastante bonito, principalmente essa borda lateral direita.

Criando novo corpo neste [link](https://google.com.br) do google

* Item da lista 1
* Item 2
* Item 3

# Conclusão

Corpo da conclusão do meu texto
