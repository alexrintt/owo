const Content = [
    {
        label: "Home",
        url: "/"
    },
    {
        label: "Pesquisar",
        url: "/search"
    },
    {
        label: "Agradecimentos",
        url: "/obrigado"
    }
]

export default Content