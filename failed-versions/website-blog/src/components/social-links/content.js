import { Adobe } from "styled-icons/boxicons-logos/Adobe"
import { Android } from "styled-icons/boxicons-logos/Android"
import { Angular } from "styled-icons/boxicons-logos/Angular"
import { Apple } from "styled-icons/boxicons-logos/Apple"

const Content = [
    {
        label: "Adobe",
        url: "https://google.com.br",
        icon: Adobe
    },
    {
        label: "Android",
        url: "https://google.com.br",
        icon: Android
    },
    {
        label: "Angular",
        url: "https://google.com.br",
        icon: Angular
    },
    {
        label: "Apple",
        url: "https://google.com.br",
        icon: Apple
    }
]

export default Content