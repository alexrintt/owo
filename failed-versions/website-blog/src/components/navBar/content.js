import { Home } from "styled-icons/boxicons-regular/Home"
import { SearchAlt as Search } from "styled-icons/boxicons-regular/SearchAlt"

const Content = [
    {
        label: "Home",
        url: "/",
        icon: Home
    },
    {
        label: "Search",
        url: "/search",
        icon: Search
    }
]

export default Content