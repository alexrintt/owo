import "package:flutter_starter_kit/src/screens/home/home.dart";

class AppRoutes {
  static routes() {
    return {
      '/': (context) => HomeScreen(),
    };
  }
}
