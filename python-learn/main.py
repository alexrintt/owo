class Math:
    @staticmethod
    def subtract(n1, n2):
        """
        Subtract two numbers
        """
        return n1 - n2

    @staticmethod
    def sum(n1, n2):
        """
        Sum two numbers
        """
        return n1 + n2


print(Math.sum(1, 2))
