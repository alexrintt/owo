const fs = require("fs");
const http = require("http");

function download(url, dest, cb) {
  const file = fs.createWriteStream(dest);

  const request = http.get(
    {
      hostname: "load1.servermonster.online",
      path: "/assistironline/kubikiri_cycle_aoiro_savant_to_zaregototsukai/kubikiri_cycle_aoiro_savant_to_zaregototsukai-episodio-8.mp4",
      headers: {
        referer: "https://animesonline.vip/",
      },
    },
    (response) => {
      // check if response is success
      if (response.statusCode !== 200) {
        if (response.statusCode === 301 || response.statusCode === 302) {
          console.log("Redirect to: " + response.headers.location);
          return download(response.headers.location, dest, cb);
        }

        return cb("Response status was " + response.statusCode);
      }
      console.log("Something is happening");
      response.pipe(file);
    }
  );

  // close() is async, call cb after close completes
  file.on("finish", () => file.close(cb));

  // check for request error too
  request.on("error", (err) => {
    fs.unlink(dest, () => cb(err.message)); // delete the (partial) file and then return the error
  });

  file.on("error", (err) => {
    // Handle errors
    fs.unlink(dest, () => cb(err.message)); // delete the (partial) file and then return the error
  });
}

(async () => {
  download(
    "https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png",
    "episode.mp4",
    (err) => {
      console.log(err);
    }
  );
})();
