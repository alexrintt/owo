import 'package:flutter/material.dart';

const kColor06 = Color(0xfff0fac8);
const kColor05 = Color(0xfffaaa5a);
const kColor04 = Color(0xfff76c59);
const kColor03 = Color(0xfff04181);
const kColor02 = Color(0xff981fab);
const kColor01 = Color(0xff431480);
const kColor00 = Color(0xff07102e);
