import 'dart:async';
import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import 'package:coral/theme/colors.dart';
import 'package:coral/widget/swipe_detector.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await SystemChrome.setPreferredOrientations(
    [
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ],
  );

  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(
      statusBarColor: kColor02,
    ),
  );

  await SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);

  runApp(const App());
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: const HomePage(),
    );
  }
}

class HomePage extends HookWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String? swipeDirection;
    final Map<AnimationController, AxisDirection> cancelledAnimations = {};

    final boardConfig = useState<BoardConfiguration>(
      const BoardConfiguration(
        columns: 20,
        rows: 8,
      ),
    );
    final board = useValueNotifier<Board>(
      Board(
        snakes: [
          Snake(
            nextDirection: AxisDirection.down,
            config: boardConfig.value,
            nodes: [
              SnakeNode(
                origin: Offset.zero,
                progress: 0,
                direction: AxisDirection.right,
                config: boardConfig.value,
              ),
              SnakeNode(
                origin: const Offset(1, 0),
                progress: 0,
                direction: AxisDirection.right,
                config: boardConfig.value,
              ),
              SnakeNode(
                origin: const Offset(2, 0),
                progress: 0,
                direction: AxisDirection.right,
                config: boardConfig.value,
              ),
              SnakeNode(
                origin: const Offset(3, 0),
                progress: 0,
                direction: AxisDirection.right,
                config: boardConfig.value,
              ),
              SnakeNode(
                origin: const Offset(4, 0),
                progress: 0,
                direction: AxisDirection.down,
                config: boardConfig.value,
              ),
            ],
          ),
        ],
      ),
    );

    final snakeControllers = useValueNotifier<List<AnimationController>>([
      for (final _ in board.value.snakes)
        useAnimationController(duration: const Duration(milliseconds: 300)),
    ]);

    final glasses =
        useValueNotifier<Map<AnimationController, Animation<double>>>(
      {
        for (var i = 0; i < 5; i++)
          ...(() {
            final controller = useAnimationController(
              duration: const Duration(seconds: 20),
            );

            final animation =
                CurvedAnimation(parent: controller, curve: Curves.easeInOut);

            return {
              controller: animation,
            };
          })(),
      },
    );

    useEffect(
      () {
        void updateGlasses() {
          for (final glass in glasses.value.entries) {
            final controller = glass.key;

            Future<void> loop() async {
              await controller.forward(from: 0);

              glasses.value.update(
                controller,
                (value) => CurvedAnimation(
                  parent: controller,
                  curve: [
                    Curves.bounceIn,
                    Curves.bounceInOut,
                    Curves.bounceOut,
                    Curves.decelerate,
                    Curves.ease,
                    Curves.easeIn,
                    Curves.easeInBack,
                    Curves.easeInCirc,
                    Curves.easeInCirc,
                    Curves.easeInCubic,
                    Curves.easeInExpo,
                  ][Random().nextInt(11)],
                ),
              );

              loop();
            }

            loop();
          }
        }

        void updateSnakes() {
          for (final snakeController in snakeControllers.value) {
            final index = snakeControllers.value.indexOf(snakeController);

            Future<bool> startToEnd() {
              final completer = Completer<bool>();

              snakeController.forward(from: 0).orCancel.then(
                    (_) => completer.complete(false),
                    onError: (_) => completer.complete(true),
                  );

              return completer.future;
            }

            snakeController.addListener(() {
              final snake = board.value.snakes[index];

              if (snakeController.status == AnimationStatus.forward) {
                if (cancelledAnimations[snakeController] != null) {
                  final cancelledBy = cancelledAnimations[snakeController]!;

                  board.value = board.value.copyWith(
                    snakes: [...board.value.snakes]
                      ..removeAt(index)
                      ..insert(
                        index,
                        board.value.snakes[index].cancel(cancelledBy),
                      ),
                  );

                  cancelledAnimations.remove(snakeController);

                  snakeController.reset();
                } else {
                  board.value = board.value.copyWith(
                    snakes: [...board.value.snakes]
                      ..removeAt(index)
                      ..insert(
                        index,
                        snake.move(snakeController.value),
                      ),
                  );
                }
              }
            });

            void loop({required bool canceled}) {
              if (!cancelledAnimations.containsKey(snakeController)) {
                board.value = board.value.copyWith(
                  snakes: [...board.value.snakes]
                    ..removeAt(index)
                    ..insert(
                      index,
                      canceled
                          ? board.value.snakes[index]
                          : board.value.snakes[index].locked(),
                    ),
                );
              }

              startToEnd().then((canceled) => loop(canceled: canceled));
            }

            startToEnd().then((canceled) => loop(canceled: canceled));
          }
        }

        updateGlasses();
        updateSnakes();

        return () {};
      },
      const [],
    );

    void updateDirection(AxisDirection direction) {
      for (final snakeController in snakeControllers.value) {
        final index = snakeControllers.value.indexOf(snakeController);
        final snake = board.value.snakes[index];

        // final canCancelToThisNextDirection = (() {
        //   if (snake.body.isEmpty) return true;

        //   return snake.body.last.direction.opposite != direction;
        // })();

        board.value = board.value.copyWith(
          snakes: [...board.value.snakes]
            ..removeAt(index)
            ..insert(index, snake.copyWith(nextDirection: direction)),
        );

        if (snakeController.value <= 0.1) {
          cancelledAnimations[snakeController] = direction;
        }
      }
    }

    return SwipeDetector(
      onSwipeDown: () => updateDirection(AxisDirection.down),
      onSwipeLeft: () => updateDirection(AxisDirection.left),
      onSwipeRight: () => updateDirection(AxisDirection.right),
      onSwipeUp: () => updateDirection(AxisDirection.up),
      child: ColoredBox(
        color: kColor01,
        child: Stack(
          children: [
            Positioned.fill(
              child: CustomPaint(
                child: AnimatedBuilder(
                  animation: Listenable.merge(glasses.value.keys.toList()),
                  builder: (context, child) {
                    return CustomPaint(
                      painter: GlassesPainter(
                        glasses:
                            glasses.value.values.map((e) => e.value).toList(),
                      ),
                    );
                  },
                ),
              ),
            ),
            Positioned.fill(
              child: SafeArea(
                child: AnimatedBuilder(
                  animation: board,
                  builder: (context, child) {
                    return CustomPaint(
                      painter: BoardPainter(
                        board: board.value,
                        boardConfig: boardConfig.value,
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

extension AxisDirectionUtil on AxisDirection {
  bool get isVertical => [AxisDirection.up, AxisDirection.down].contains(this);
  bool get isHorizontal =>
      [AxisDirection.left, AxisDirection.right].contains(this);

  AxisDirection get adjacent {
    switch (this) {
      case AxisDirection.up:
        return AxisDirection.left;
      case AxisDirection.down:
        return AxisDirection.right;
      case AxisDirection.left:
        return AxisDirection.up;
      case AxisDirection.right:
        return AxisDirection.down;
    }
  }

  AxisDirection get opposite {
    switch (this) {
      case AxisDirection.up:
        return AxisDirection.down;
      case AxisDirection.down:
        return AxisDirection.up;
      case AxisDirection.left:
        return AxisDirection.right;
      case AxisDirection.right:
        return AxisDirection.left;
    }
  }
}

mixin DirectionMixin {
  Offset of(AxisDirection direction) {
    late Offset delta;

    switch (direction) {
      case AxisDirection.up:
        delta = const Offset(0, -1);
        break;
      case AxisDirection.down:
        delta = const Offset(0, 1);
        break;
      case AxisDirection.left:
        delta = const Offset(-1, 0);
        break;
      case AxisDirection.right:
        delta = const Offset(1, 0);
        break;
    }

    return delta;
  }
}

class BoardConfiguration {
  const BoardConfiguration({
    required this.columns,
    required this.rows,
  });

  final int columns;
  final int rows;
}

class SnakeNode with DirectionMixin {
  const SnakeNode({
    required this.origin,
    required this.progress,
    required this.direction,
    required this.config,
  });

  final Offset origin;
  final double progress;
  final AxisDirection direction;
  final BoardConfiguration config;

  List<Offset> get positions {
    final delta = of(direction);

    final position = origin + delta * progress;

    final shadows = [
      if (position.dx < 0) position.translate(config.columns / 1, 0),
    ];

    return [position, ...shadows];
  }

  Offset get realPosition {
    final delta = of(direction);

    return origin + delta * progress;
  }

  SnakeNode locked(AxisDirection nextDirection) {
    if (realPosition.dx < 0) {
      return copyWith(
        origin: Offset(config.columns / 1, realPosition.dy),
        progress: 0,
        direction: nextDirection,
      );
    }

    return copyWith(
      origin: origin + of(direction),
      direction: nextDirection,
      progress: 0,
    );
  }

  SnakeNode cancel([AxisDirection? newDirection]) {
    return copyWith(
      progress: 0,
      direction: newDirection,
    );
  }

  SnakeNode copyWith({
    Offset? origin,
    double? progress,
    AxisDirection? direction,
    BoardConfiguration? config,
  }) {
    return SnakeNode(
      origin: origin ?? this.origin,
      progress: progress ?? this.progress,
      direction: direction ?? this.direction,
      config: config ?? this.config,
    );
  }
}

class Snake {
  const Snake({
    required this.config,
    required this.nodes,
    required this.nextDirection,
  });

  final List<SnakeNode> nodes;
  final BoardConfiguration config;
  final AxisDirection nextDirection;

  SnakeNode get head => nodes.last;
  List<SnakeNode> get body => nodes.take(nodes.length - 1).toList();

  Snake move(double progress) {
    return copyWith(
      nodes: [
        ...nodes.map((e) => e.copyWith(progress: progress)),
      ],
    );
  }

  Snake locked() {
    return copyWith(
      nodes: [
        for (final node in nodes.sublist(0, nodes.length - 1))
          node.locked(nextOf(node)!.direction),
        head.locked(nextDirection),
      ],
    );
  }

  SnakeNode? nextOf(SnakeNode node) => nodeOf(node, 1);
  SnakeNode? previousOf(SnakeNode node) => nodeOf(node, -1);
  SnakeNode? nodeOf(SnakeNode node, int move) =>
      nodes.elementAt(nodes.indexOf(node) + move);

  Snake cancel(AxisDirection headNextDirection) {
    return copyWith(
      nextDirection: headNextDirection,
      nodes: [
        for (final node in nodes.sublist(0, nodes.length - 1)) node.cancel(),
        head.cancel(headNextDirection),
      ],
    );
  }

  Snake copyWith({
    BoardConfiguration? config,
    List<SnakeNode>? nodes,
    AxisDirection? nextDirection,
  }) {
    return Snake(
      config: config ?? this.config,
      nodes: nodes != null ? [...nodes] : [...this.nodes],
      nextDirection: nextDirection ?? this.nextDirection,
    );
  }
}

class Board {
  const Board({this.snakes = const []});

  const Board.empty() : snakes = const [];

  final List<Snake> snakes;

  Board copyWith({
    List<Snake>? snakes,
  }) {
    return Board(
      snakes: snakes != null ? [...snakes] : [...this.snakes],
    );
  }
}

class BoardBounds {
  const BoardBounds({required this.size, required this.config});

  final Size size;
  final BoardConfiguration config;

  Size get cell => Size(size.width / config.columns, size.height / config.rows);
  double get cellSize => cell.shortestSide;
  Offset get offset => Offset(
        (size.width - cellSize * config.columns) / 2,
        (size.height - cellSize * config.rows) / 2,
      );
}

class GlassesPainter extends CustomPainter {
  const GlassesPainter({required this.glasses});

  final List<double> glasses;

  Offset _width(Size size) => Offset(size.width / 10, 0);
  Offset _translation(Size size) => Offset(size.width / 5, 0);

  List<Offset> _generateGlassPolygon(Size size) {
    final width = _width(size);
    final translation = _translation(size);

    final bottomEnd = Offset.zero + Offset(0, size.height);
    final bottomStart = bottomEnd - width;
    final topEnd = bottomStart - translation - Offset(0, size.height);
    final topStart = topEnd - width;

    return [
      topStart,
      topEnd,
      bottomEnd,
      bottomStart,
      topStart,
    ];
  }

  Path _drawGlass(double progress, Size size, [Path? base]) {
    final width = _width(size);
    final translation = _translation(size);

    final path = base ?? Path();

    path.addPolygon(_generateGlassPolygon(size), true);

    return path.shift(
      Offset((size.width + translation.dx + width.dx * 2) * progress, 0),
    );
  }

  @override
  void paint(Canvas canvas, Size size) {
    var glassesPath = Path();
    final glassesPaint = Paint()
      ..color = Colors.white.withOpacity(.02)
      ..style = PaintingStyle.fill
      ..strokeWidth = 2;

    for (final progress in glasses) {
      glassesPath = _drawGlass(progress, size, glassesPath);
    }

    canvas.drawPath(glassesPath, glassesPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}

class BoardPainter extends CustomPainter {
  const BoardPainter({
    required this.board,
    required this.boardConfig,
  });

  final Board board;
  final BoardConfiguration boardConfig;

  Path _drawPoint(Offset point, [Path? base]) {
    return (base ?? Path())
      ..addRRect(
        RRect.fromRectAndRadius(
          Rect.fromCenter(center: point, width: 10, height: 10),
          const Radius.circular(5),
        ),
      );
  }

  Path _drawBoard(Board board, Size size, [Path? base]) {
    final path = base ?? Path();

    path.addRect(Rect.fromLTWH(0, 0, size.width, size.height));

    return path;
  }

  Path _drawSnake(Snake snake, BoardBounds size, [Path? base]) {
    final path = base ?? Path();

    for (final node in snake.nodes) {
      _drawSnakeNode(node, size, path);
    }

    return path;
  }

  Path _drawSnakeNode(SnakeNode node, BoardBounds bounds, [Path? base]) {
    final path = base ?? Path();

    for (final position in node.positions) {
      path.addRect(
        Rect.fromLTWH(
          position.dx * bounds.cellSize + bounds.offset.dx,
          position.dy * bounds.cellSize + bounds.offset.dy,
          bounds.cellSize,
          bounds.cellSize,
        ),
      );
    }

    return path;
  }

  Path _drawSnakeDecoration(Snake snake, BoardBounds size, [Path? base]) {
    final path = base ?? Path();

    for (final node in snake.nodes) {
      _drawSnakeNodeDecoration(
        node,
        size,
        path,
      );
    }

    return path;
  }

  Path _drawSnakeNodeDecoration(
    SnakeNode node,
    BoardBounds bounds, [
    Path? base,
  ]) {
    final path = base ?? Path();

    for (final position in node.positions) {
      path.addRRect(
        RRect.fromRectAndRadius(
          Rect.fromLTWH(
            (position.dx * bounds.cellSize + bounds.offset.dx) +
                bounds.cellSize / 2 / 2,
            (position.dy * bounds.cellSize + bounds.offset.dy) +
                bounds.cellSize / 2 / 2,
            bounds.cellSize / 2,
            bounds.cellSize / 2,
          ),
          const Radius.circular(2),
        ),
      );
    }

    return path;
  }

  @override
  void paint(Canvas canvas, Size size) {
    final path = Path();
    final paint = Paint()
      ..color = kColor04
      ..style = PaintingStyle.fill
      ..strokeWidth = 2;

    final snakeDecorationPath = Path();
    final snakeDecorationPaint = Paint()
      ..color = kColor05
      ..style = PaintingStyle.fill
      ..strokeWidth = 2;

    final boardPath = Path();
    final boardPaint = Paint()
      ..color = kColor00
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2;

    final dotsPath = Path();
    final dotsPaint = Paint()
      ..color = kColor00
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2;

    final padding = size.shortestSide / 10;
    final offset = Offset(padding, padding);
    final halfOffset = offset / 2;

    final boardCanvasSize =
        Size(size.width - offset.dx, size.height - offset.dy);

    final bounds = BoardBounds(config: boardConfig, size: boardCanvasSize);

    for (final snake in board.snakes) {
      _drawSnake(snake, bounds, path);
      _drawSnakeDecoration(snake, bounds, snakeDecorationPath);
    }

    for (var i = 0; i < boardConfig.columns; i++) {
      for (var j = 0; j < boardConfig.rows; j++) {
        _drawPoint(
          Offset(i * bounds.cellSize, j * bounds.cellSize) +
              bounds.offset +
              Offset(bounds.cellSize / 2, bounds.cellSize / 2),
          dotsPath,
        );
      }
    }

    _drawBoard(board, boardCanvasSize, boardPath);

    canvas
      ..drawPath(boardPath.shift(halfOffset), boardPaint)
      ..drawPath(dotsPath.shift(halfOffset), dotsPaint)
      ..drawPath(path.shift(halfOffset), paint)
      ..drawPath(snakeDecorationPath.shift(halfOffset), snakeDecorationPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}
