package io.lakscastro.hiyori.auth

interface AuthClient {
  suspend fun signIn(): String
  suspend fun signOut(): Boolean
}