package io.lakscastro.hiyori

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.squareup.moshi.Json
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import okhttp3.Request
import okio.IOException


class MainActivity : AppCompatActivity() {
  private val client by lazy { OkHttpClient() }
  private val moshi = Moshi.Builder().addLast(KotlinJsonAdapterFactory()).build()
  private val myAnimeListJsonAdapter = moshi.adapter(MyAnimeListResponse::class.java)

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    val config =
      ImageLoaderConfiguration.Builder(this).denyCacheImageMultipleSizesInMemory().build()

    ImageLoader.getInstance().init(config);

    renderOAuthButton()
  }

  private fun renderLoadingIndicator() {
    setContentView(R.layout.full_loading)
  }

  private fun renderOAuthButton() {
    setContentView(R.layout.oauth_button)

    val button = findViewById<Button>(R.id.signInWith)

    button.setOnClickListener {
      renderLoadingIndicator()

      GlobalScope.launch(Dispatchers.IO) {
        val animeList = fetchPopularAnime()

        for (item in animeList!!.data!!) {
          Log.d("${item.node.id}", item.node.title)
        }

        GlobalScope.launch(Dispatchers.Main) {
          renderRecyclerView(animeList.data.map { it.node })
        }
      }
    }
  }

  private fun fetchPopularAnime(): MyAnimeListResponse? {
    val request =
      Request.Builder().url("https://api.myanimelist.net/v2/anime?q=one&limit=100")
        .addHeader("X-MAL-CLIENT-ID", "495685aa929f5f4f47451674e96d109a").build()

    client.newCall(request).execute().use { response ->
      if (!response.isSuccessful) throw IOException("Unexpected code $response")

      return myAnimeListJsonAdapter.fromJson(response.body!!.source())
    }
  }

  private fun renderRecyclerView(animeList: List<AnimeInfo>) {
    setContentView(R.layout.anime_list)
    val adapter = CustomAdapter(animeList)

    val recyclerView: RecyclerView = findViewById(R.id.recycler_view)
    recyclerView.adapter = adapter
  }
}

class CustomAdapter(private val dataSet: List<AnimeInfo>) :
  RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

  /**
   * Provide a reference to the type of views that you are using
   * (custom ViewHolder).
   */
  class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val cardView: CardView = view.findViewById(R.id.itemCard)
    private val titleView: TextView = view.findViewById(R.id.cardTitle)
    private val imageView: ImageView = view.findViewById(R.id.imageView)
    private val descriptionView: TextView = view.findViewById(R.id.cardDescription)

    init {
      cardView.setOnClickListener {
        Log.d("asdf", "SDOKFHSDO ${titleView.text}")
      }
    }

    fun bind(title: String, description: String, imageUrl: String) {
      val imageLoader = ImageLoader.getInstance()
      val imageOptions =
        DisplayImageOptions.Builder()
          .resetViewBeforeLoading(true)
          .cacheInMemory(true)
          .showImageOnLoading(R.drawable.ic_launcher_background)
          .build()

      titleView.text = title
      descriptionView.text = description
      imageLoader.displayImage(imageUrl, imageView, imageOptions)
    }
  }

  // Create new views (invoked by the layout manager)
  override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
    // Create a new view, which defines the UI of the list item
    val view = LayoutInflater.from(viewGroup.context)
      .inflate(R.layout.list_item, viewGroup, false)

    return ViewHolder(view)
  }

  // Replace the contents of a view (invoked by the layout manager)
  override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
    // Get element from your dataset at this position and replace the
    // contents of the view with that element
    val current = dataSet[position]

    viewHolder.bind(
      current.title,
      "${current.title} has ID: ${current.id}",
      current.mainPicture.medium,
    )
  }

  // Return the size of your dataset (invoked by the layout manager)
  override fun getItemCount() = dataSet.size
}

data class MyAnimeListResponse(val data: List<MyAnimeListNode>)

data class MyAnimeListNode(val node: AnimeInfo)

data class AnimeInfo(
  val id: Int,
  val title: String,
  @Json(name = "main_picture") val mainPicture: Picture
)

data class Picture(val medium: String, val large: String)