package io.lakscastro.hiyori.auth

interface ImplicitOAuthClient : AuthClient {
  interface EndpointHandler {
    suspend fun tokenEndpoint()
    suspend fun authorizationEndpoint()
  }

  suspend fun createEndpointHandler()
}