package io.lakscastro.wifidirect.ui.theme

import androidx.compose.ui.graphics.Color

val BorderColor = Color(0xFF444C56)
val PaperColor = Color(0xFF22272E)
val BackgroundColor = Color(0xFF1C2128)

val PrimaryColor = Color(0xFFC53243)
val PrimaryVariantColor = Color(0xFF9D2532)

val SecondaryColor = Color(0xFF894CED)
val SecondaryVariantColor = Color(0xFF8244EA)