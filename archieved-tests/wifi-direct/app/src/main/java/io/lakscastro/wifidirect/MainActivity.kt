package io.lakscastro.wifidirect

import android.content.Context
import android.net.wifi.p2p.WifiP2pManager
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionRequired
import com.google.accompanist.permissions.rememberPermissionState
import io.lakscastro.wifidirect.ui.theme.WiFiDirectTheme


class MainActivity : ComponentActivity() {
  @ExperimentalPermissionsApi
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContent {
      HomePage()
    }
  }
}

@ExperimentalPermissionsApi
@Composable
fun HomePage() {
  val (isLoading, setIsLoading) = remember { mutableStateOf(true) }
  val context = LocalContext.current
  val manager = context.getSystemService(Context.WIFI_P2P_SERVICE) as WifiP2pManager

  val requestPermissions =
    rememberLauncherForActivityResult(contract = ActivityResultContracts.RequestPermission()) { isGranted ->
      if (isGranted) {
        Log.d("ASDF", "Permission granted")
      } else {
        Log.d("ASDF", "Permission denied")
      }
    }

//  val launcher =
//    rememberLauncherForActivityResult(contract = ActivityResultContracts.StartActivityForResult()) { result ->
//      val uri = result.data?.data.toString()
//
//      Log.d("kekek", uri)
//    }

  LaunchedEffect(Unit) {
//    BroadcastReceiver
  }


  WiFiDirectTheme {
    // A surface container using the 'background' color from the theme
    RequiresCameraPermission {
      Scaffold {
        Box(
          modifier = Modifier.fillMaxSize(),
          contentAlignment = Alignment.Center
        ) {
          if (isLoading) {
            Text(
              "Abrir telinha pra pegar arquivos",
              modifier = Modifier.clickable {

              },
            )
          } else {
            Text("Carregou!!!")
          }
        }
      }
    }
  }
}

@ExperimentalPermissionsApi
@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
  HomePage()
}

@ExperimentalPermissionsApi
@Composable
private fun RequiresCameraPermission(block: @Composable () -> Unit) {
// Track if the user doesn't want to see the rationale any more.
  var doNotShowRationale by rememberSaveable { mutableStateOf(false) }

  val cameraPermissionState = rememberPermissionState(android.Manifest.permission.ACCESS_FINE_LOCATION)
  PermissionRequired(
    permissionState = cameraPermissionState,
    permissionNotGrantedContent = {
      if (doNotShowRationale) {
        Text("Feature not available")
      } else {
        Column {
          Text("The camera is important for this app. Please grant the permission.")
          Spacer(modifier = Modifier.height(8.dp))
          Row {
            Button(onClick = { cameraPermissionState.launchPermissionRequest() }) {
              Text("Ok!")
            }
            Spacer(Modifier.width(8.dp))
            Button(onClick = { doNotShowRationale = true }) {
              Text("Nope")
            }
          }
        }
      }
    },
    permissionNotAvailableContent = {
      Column {
        Text(
          "Camera permission denied. See this FAQ with information about why we " +
            "need this permission. Please, grant us access on the Settings screen."
        )
        Spacer(modifier = Modifier.height(8.dp))
        Button(onClick = {}) {
          Text("Open Settings")
        }
      }
    }
  ) {
    Text("Camera permission Granted")
  }
}