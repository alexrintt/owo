const lines: Array<string> = [
  "2",
  "4",
  "A1 C2",
  "B3 C3",
  "C4 C2",
  "C2 D2",
  "2",
  "A1 A5",
  "E1 E5",
];

while (true) {
  const graphLength: number | undefined = Number(lines.shift());

  if (!graphLength) break;
}
